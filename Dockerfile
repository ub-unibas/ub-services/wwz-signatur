FROM php:apache

# Install MySQLi extension
RUN docker-php-ext-install mysqli

# copy scripts to webroot
COPY scripts/ /var/www/html/
