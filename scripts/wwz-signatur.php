<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- 

  wwz-signatur.php
  Verwalte Signaturen und Numerus Currens in Datenbank wwz.wwzsignatur.
  
  Offenbar ist Aleph zu doof, um für die Signaturen vom Typ "RH 123, 359"
  einen n.c. zu vergeben. Oder die Bibliothek ist zu wenig flexibel und
  kann ihre Signaturstruktur nicht anpassen. Wie dem auch sei: Seit der
  Einführung von Aleph läuft dieses kleine Skript, das so gut wie keine
  Arbeit macht.

  Geschichte:
    00.00.1999: Version als Perlskript mit Textdatei
    29.09.2004: Rewrite für MySQL und PHP
    04.10.2011: Zugangskontrolle geschieht auf Stufe Apache; Feld "ip" ignoriert
    20.09.2013: Namensänderung der Bibliothek. Ja mei...
    02.02.2015: mysql_real_escape_string() statt mysql_escape_string()
    29.02.2024: PHP8 ready und bitz aufräumen
    29.08.2024: use SSL for mariadb

-->
<html>
<head>
<title>WWZ Bibliothek: Signaturserver</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<style type="text/css">
body,td,p,form,div { font-family:Arial; }
</style>
<script language="JavaScript" type="text/javascript">
function require(formnr,message) {
  if (document.forms[formnr].elements[0].value.length > 0)
    return true;
  else {
    alert(message);
    document.forms[formnr].elements[0].focus();
    return false;
  }
}
</script>
</head>
<body onLoad="document.forms[0].elements[0].focus()">
<table summary="" width="100%" cellspacing="0" cellpadding="5" bgcolor="#FFCC66">
 <tr>
   <td><big>UB Wirtschaft: Signaturserver</big></td>
   <td style="text-align:right"><a href="https://intranet.unibas.ch/display/UBIT/SWA-WWZ+Signaturserver">Hilfe</a></td>
 </tr>
</table>
<?php
////////////////////////////////////////////////////////////////////////////

// Verbindungsparameter
$DBHOST     = 'ub-mariadb01.ub.unibas.ch';
$DB         = 'wwz';
$UID        = 'wwz';
$PWD        = getenv('WWZ_DB_PWD');

// SSL-Parameter
$db_ca_file = '/etc/ssl/certs/ca-certificates.crt';
$db_ca_path = '/etc/ssl/certs';

// variabeln initiieren
$URL = $_SERVER['PHP_SELF'];
$VORSPANN = '';

// Überprüfen, ob die Aktion gesetzt ist
$ACTION = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

if ($ACTION) {
    $prefix = trim($_REQUEST['prefix']);

    // mariadb-Verbindung mit SSL herstellen
    try {
        $conn = mysqli_init();
        $conn->ssl_set(NULL, NULL, $db_ca_file, $db_ca_path , NULL);
        $conn->options(MYSQLI_CLIENT_SSL, true);
        $conn->real_connect($DBHOST, $UID, $PWD, $DB);
    } catch (mysqli_sql_exception $e) {
        die("Connection failed: " . $e->getMessage());
    }

    // Verbindungsfehler überprüfen
    if ($conn->connect_error) {
        die("Verbindung fehlgeschlagen: " . $conn->connect_error);
    }

    $sql_prefix = $conn->real_escape_string($prefix); // Sonderzeichen escapen

    if ($ACTION == 'lookup') {

        // Präfix nachschlagen
        $sql = "SELECT * FROM wwzsignatur WHERE prefix='$sql_prefix'";
        $res = $conn->query($sql);

        if ($res && $res->num_rows > 0) {
            // Signatur gefunden: nc um eins erhöhen
            $row = $res->fetch_assoc();
            $nc = $row['nc'] + 1;

            $sql = "UPDATE wwzsignatur SET nc=$nc WHERE prefix='$sql_prefix'";
            $conn->query($sql);

            echo "<p>Neuer n.c: <big><big><b>&nbsp;&nbsp;$prefix $nc</b></big></big></p>";
            echo "<hr noshade size='1'>";
        } else {
            // Signatur nicht gefunden
            // Eingabe korrigieren oder neu hinzufügen
            $VORSPANN = $prefix;

            echo "<p>Signatur <b>$prefix</b> nicht in Datenbank. Tippfehler?</p>";
            echo "<hr noshade size='1'>";
            echo "<p>Eingabe korrigieren:</p>";
        }
    } elseif ($ACTION == 'add') {
        $nc = $_REQUEST['nc'];
        if (preg_match("/^\d+$/", $nc)) {
            // Neue Signatur einfügen
            $sql = "INSERT INTO wwzsignatur (prefix, nc) VALUES ('$sql_prefix', $nc)";
            $conn->query($sql);

            echo "<p>Signatur <b>$prefix $nc</b> in Datenbank aufgenommen.</p>";
            echo "<hr noshade size='1'>";
        } else {
            die("Nummerus currens ist nicht numerisch");
        }
    }

    // Verbindung schließen
    $conn->close();
}

// Formular für Suche
echo "<br>";
echo "<form name='lookup' method='post' action='$URL' onsubmit='return require(0, \"Bitte Signatur eingeben\")'>";
echo "Signatur: <input type='text' size='15' name='prefix' value='$VORSPANN'>";
echo "<input type='hidden' name='action' value='lookup'>";
echo "<input type='submit' value='  n.c.  ' style='margin-left:0.5cm'>";
echo "</form>";

// Formular für neue Signatur hinzufügen
if ($VORSPANN) {
    echo "<hr noshade size='1'>";
    echo "<form name='add' method='post' action='$URL' onsubmit='return require(1, \"Bitte Numerus currens eingeben\")'>";
    echo "<p>Signatur <b>$prefix</b> neu in die Datenbank aufnehmen.<br>";
    echo "Letzter Numerus currens:</p>";
    echo "<b>$prefix</b> <input type='text' size='10' name='nc'>";
    echo "<input type='hidden' name='action' value='add'>";
    echo "<input type='hidden' name='prefix' value='$prefix'>";
    echo "<input type='submit' value='neu aufnehmen' style='margin-left:0.5cm'>";
    echo "</form>";
}
?>

<hr noshade size='1'>
<div><small><small>&copy; 1999-2024 UB Basel/ava,med</small></small></div>
</body>
</html>
